﻿using System;
using System.Collections.Generic;
using System.Linq;
using Extensions;
using Gameplay.CatsWish;
using MoreMountains.Tools;
using ScriptableObjects.Classes.Wish;
using Sirenix.OdinInspector;
using UnityEngine;
using static Common.Enums;

namespace Gameplay.Providers
{
    public class WishSetupProvider : Singleton<WishSetupProvider>
    {
        public static event Action OnChangeInteractionHeap;

        [Title("Components")]
        [SerializeField] private WishesData _wishesData;

        [GroupView] private static readonly List<WishData> CurrentWishOneRoomDataList = new();
        [GroupView] private static readonly List<WishData> CurrentWishTwoRoomDataList = new();

        [GroupView] private static readonly List<CatWishBar> CatWishBarOneRoomList = new();
        [GroupView] private static readonly List<CatWishBar> CatWishBarTwoRoomList = new();

        protected override void Awake()
        {
            base.Awake();

            foreach (var wishData in _wishesData.WishDataOneRoomList)
            {
                AddWishData(wishData, RoomType.RoomOne);
            }

            foreach (var wishData in _wishesData.WishDataTwoRoomList)
            {
                AddWishData(wishData, RoomType.RoomTwo);
            }

            ShuffleWish();
        }

        private static void ShuffleWish()
        {
            CurrentWishOneRoomDataList.MMShuffle();
            CurrentWishTwoRoomDataList.MMShuffle();
        }

        public static WishData GetWishData(RoomType roomType)
        {
            WishData wishData = default;
            switch (roomType)
            {
                case RoomType.RoomOne:
                    wishData = CurrentWishOneRoomDataList.First(x => x.TypeWish != WishType.SingCat);
                    RemoveWishData(wishData, roomType);
                    break;
                case RoomType.RoomTwo:
                    wishData = CurrentWishTwoRoomDataList.First(x => x.TypeWish != WishType.SingCat);
                    RemoveWishData(wishData, roomType);
                    break;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }

            return wishData;
        }

        public static void AddWishData(WishData wishData, RoomType roomType)
        {
            switch (roomType)
            {
                case RoomType.RoomOne:
                    if (!CurrentWishOneRoomDataList.Contains(wishData))
                    {
                        CurrentWishOneRoomDataList.Add(wishData);
                    }

                    break;

                case RoomType.RoomTwo:
                    if (!CurrentWishTwoRoomDataList.Contains(wishData))
                    {
                        CurrentWishTwoRoomDataList.Add(wishData);
                    }

                    break;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }
        }

        public static void WishBarOrderByTime(CatWishBar catWishBar, RoomType roomType)
        {
            switch (roomType)
            {
                case RoomType.RoomOne:
                {
                    if (!CatWishBarOneRoomList.Contains(catWishBar))
                    {
                        CatWishBarOneRoomList.Add(catWishBar);
                    }

                    break;
                }
                case RoomType.RoomTwo:
                    if (!CatWishBarTwoRoomList.Contains(catWishBar))
                    {
                        CatWishBarTwoRoomList.Add(catWishBar);
                    }

                    break;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }
        }

        public static void RemoveWishBarOrderByTime(CatWishBar catWishBar, RoomType roomType)
        {
            switch (roomType)
            {
                case RoomType.RoomOne:
                    if (!CatWishBarOneRoomList.Contains(catWishBar)) return;
                    CatWishBarOneRoomList.Remove(catWishBar);
                    break;
                case RoomType.RoomTwo:
                    if (!CatWishBarTwoRoomList.Contains(catWishBar)) return;
                    CatWishBarTwoRoomList.Remove(catWishBar);
                    break;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }
            OnChangeInteractionHeap?.Invoke();
        }

        public static CatWishBar GetCatWishBar(RoomType roomType)
        {
            CatWishBar catWishBar = default;
            switch (roomType)
            {
                case RoomType.RoomOne:
                    if (CatWishBarOneRoomList.Count == default) return default;
                    catWishBar = CatWishBarOneRoomList.Aggregate((x, y) => x.CurrentValue < y.CurrentValue ? x : y);
                    break;
                case RoomType.RoomTwo:
                    if (CatWishBarTwoRoomList.Count == default) return default;
                    catWishBar = CatWishBarTwoRoomList.Aggregate((x, y) => x.CurrentValue < y.CurrentValue ? x : y);
                    break;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }
            
            
            return catWishBar;
        }

        public static bool CheckForWish(RoomType roomType)
        {
            switch (roomType)
            {
                case RoomType.RoomOne:
                    return CatWishBarOneRoomList.Count != default;
                case RoomType.RoomTwo:
                    return CatWishBarTwoRoomList.Count != default;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }
            
            return false;
        }

        public static bool CheckForNotWish(RoomType roomType)
        {
            switch (roomType)
            {
                case RoomType.RoomOne:
                    return CatWishBarOneRoomList.Count == default;
                case RoomType.RoomTwo:
                    return CatWishBarTwoRoomList.Count == default;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }
            
            return false;
        }

        private static void RemoveWishData(WishData wishData, RoomType roomType)
        {
            switch (roomType)
            {
                case RoomType.RoomOne:
                {
                    if (CurrentWishOneRoomDataList.Contains(wishData))
                    {
                        CurrentWishOneRoomDataList.Remove(wishData);
                    }

                    break;
                }
                case RoomType.RoomTwo:
                    if (CurrentWishTwoRoomDataList.Contains(wishData))
                    {
                        CurrentWishTwoRoomDataList.Remove(wishData);
                    }

                    break;
                case RoomType.RoomThree:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(roomType), roomType, null);
            }
        }
    }
}