﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.ObjectPool;
using Extensions;
using Gameplay.Providers;
using Gameplay.Queue.PointInQueue;
using Gameplay.Triggers;
using Gameplay.Visitors;
using Sirenix.OdinInspector;
using UnityEngine;
using static Common.Enums;

namespace Gameplay.Queue
{
    public class VisitorsQueue : SerializedMonoBehaviour
    {
        public static event Action<CatType> OnReceivingType;
        public static event Action OnVisitorTutorial;


        [Title("Points Interaction")]
        [SerializeField] private Transform _spawnPoint;
        [SerializeField] private Transform _exitReceiptPoint;
        [SerializeField] private Transform _exitPreReceiptPoint;
        [SerializeField] private Transform _exitReceivingPoint;
        [SerializeField] private Transform _receptionPos;
        [Title("Spawn Points")]
        [SerializeField] private Transform _startSpawnPos;
        [SerializeField] private Transform _nextSpawnPos;
        [Title("Spawn Tutorial Points")]
        [SerializeField] private Transform _tutorialSpawnPos;
        [Title("Points Queue")]
        [SerializeField] private List<PointQueue> _pointsReceptionQueue;
        [SerializeField] private List<PointQueue> _pointsReceivingQueue;
        [Title("Visitors Settings")]
        [SerializeField] private int _maxReceiptVisitor = 1;
        [SerializeField] private int _maxReceivingVisitor = 1;


        [GroupView] private int _countSpawnVisitorInReceipt;
        [GroupView] private readonly Queue<CatType> _receiptCats = new();
        [Space]
        [GroupView] private int _countSpawnVisitorInReceiving;
        [GroupView] private readonly Queue<CatType> _receivingCats = new();

        [GroupView] private Visitor _lastReceiptVisitor;
        [GroupView] private Visitor _lastReceivingVisitor;
        public Visitor LastReceiptVisitor => _lastReceiptVisitor;
        public Visitor LastReceivingVisitor => _lastReceivingVisitor;

        [Button]
        public void GetPointsQueue()
        {
            var queuePoints = GetComponentsInChildren<PointQueue>().ToList();

            _pointsReceivingQueue = queuePoints.Where(x => x.ReceivingPoint).ToList();
            _pointsReceptionQueue = queuePoints.Where(x => !x.ReceivingPoint).ToList();
        }

        public void InitVisitorForReceipt(CatType catType, Action spawnVisitor = default)
        {
            if (_countSpawnVisitorInReceipt >= _maxReceiptVisitor)
            {
                return;
            }
            
            var pointInQueue = GetPointInQueue(_pointsReceptionQueue);
            var visitor = SpawnVisitor(catType, pointInQueue, _exitReceiptPoint, _exitPreReceiptPoint,
                true, true);
            
            _lastReceiptVisitor = visitor;
            _countSpawnVisitorInReceipt++;
          
            spawnVisitor?.Invoke();
            OnVisitorTutorial?.Invoke();
            
            visitor.OnNextPointQueue += RemoveInReceiptQueue;
        }

        private void RemoveInReceiptQueue(Visitor visitor, bool needRemove)
        {
            if (!needRemove) return;
            visitor.OnNextPointQueue -= RemoveInReceiptQueue;
            if (visitor == _lastReceiptVisitor)
            {
                _lastReceiptVisitor = default;
            }

            _countSpawnVisitorInReceipt--;
        }

        private void RemoveInReceivingQueue(Visitor visitor, bool needRemove)
        {
            if (!needRemove) return;
            visitor.OnNextPointQueue -= RemoveInReceivingQueue;
            if (visitor == _lastReceivingVisitor)
            {
                _lastReceivingVisitor = default;
            }

            _countSpawnVisitorInReceiving--;
        }

        private PointQueue GetPointInQueue(List<PointQueue> pointsQueue)
        {
            foreach (var pointQueue in pointsQueue.Where(pointQueue => !pointQueue.IsBusy))
            {
                pointQueue.IsBusy = true;
                return pointQueue;
            }

            return default;
        }

        public void InitVisitorForReceiving(CatType catType)
        {
            if (_countSpawnVisitorInReceiving >= _maxReceivingVisitor)
            {
                return;
            }

            var pointInQueue = GetPointInQueue(_pointsReceivingQueue);
            var visitor = SpawnVisitor(catType, pointInQueue, _exitReceivingPoint, _exitPreReceiptPoint, false, false);
            _lastReceivingVisitor = visitor;
            _countSpawnVisitorInReceiving++;
            visitor.OnNextPointQueue += RemoveInReceivingQueue;
            OnReceivingType?.Invoke(catType);
        }

        public void MoveSpawnPointTutorial()
        {
            _spawnPoint = _tutorialSpawnPos;
        }
        public void MoveSpawnPoint()
        {
            if (_spawnPoint.position == _nextSpawnPos.position) return;
            _spawnPoint = _nextSpawnPos;
        }

        private void Awake()
        {
            _spawnPoint = _startSpawnPos;
        }
        
        private void DeleteReceiptVisitorInQueue(Visitor visitor)
        {
            if (_receiptCats.Count > 0)
            {
                _receiptCats.Dequeue();
            }
        }

        private void DeleteReceivingVisitorInQueue(Visitor visitor)
        {
            if (_receivingCats.Count > 0)
            {
                _receivingCats.Dequeue();
            }

            if (_receivingCats.TryPeek(out CatType type))
            {
                var pointInQueue = GetPointInQueue(_pointsReceivingQueue);
                var visitorSpawn = SpawnVisitor(type, pointInQueue, _exitReceivingPoint, _exitPreReceiptPoint, false,
                    false);
                _lastReceivingVisitor = visitorSpawn;
                _countSpawnVisitorInReceiving++;
                visitorSpawn.OnNextPointQueue += RemoveInReceivingQueue;
                OnReceivingType?.Invoke(type);
            }
        }

        private Visitor SpawnVisitor(CatType catType, PointQueue point, Transform exitPoint, Transform preExitPoint,
            bool carried, bool receipt)
        {
            var prefab = PrefabProvider.GetVisitorPrefab(catType);
            var visitor = Pool.Get(prefab, _spawnPoint.position);
            visitor.SetCarried(carried);
            visitor.SetReceptionPos(_receptionPos);
            visitor.SetExitPoint(exitPoint);
            visitor.SetPreExitPoint(preExitPoint);
            visitor.PreviousVisitor(receipt ? _lastReceiptVisitor : _lastReceivingVisitor, point);
            visitor.SetDestination(point.PointTransform);
            return visitor;
        }
    }
}