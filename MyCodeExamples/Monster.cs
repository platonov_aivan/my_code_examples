﻿using System;
using System.Collections;
using Common;
using DG.Tweening;
using Extensions;
using Game.Scripts.Utils.ObjectPool;
using Gameplay.FlashingMesh;
using Gameplay.Health;
using Gameplay.Machine;
using ScriptableObjects.Classes.Levels;
using ScriptableObjects.Classes.Monsters;
using ScriptableObjects.Classes.Waves;
using Sirenix.OdinInspector;
using UI;
using UI.WindowUI;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using static Common.Enums;
using Random = UnityEngine.Random;

namespace Gameplay.Monsters
{
    [RequireComponent(typeof(PoolItem))]
    public class Monster : MonoBehaviour
    {
        public event Action<Monster> OnDied;
        public event Action<Monster> OnReleased;

        [Space]
        [SerializeField, GroupComponent] private HealthComponent _health;

        [SerializeField, GroupComponent] private Transform _shotTarget;
        [SerializeField, GroupComponent] private PoolItem _poolItem;
        [SerializeField, GroupComponent] private Flasher _flasher;

        [Space]
        [Title("Rb")]
        [SerializeField, GroupComponent] private Rigidbody _rb;

        [Space]
        [Title("Particle")]
        [SerializeField, GroupComponent] private Transform _particleTransform;

        [Space]
        [Title("Agent")]
        [SerializeField, GroupSetting] private MonsterEvent _monsterEvent;

        [SerializeField, GroupSetting] private NavMeshAgent _agent;
        [SerializeField, GroupSetting] private float _durationTurn = 5f;

        [Space]
        [Title("Trigger")]
        [SerializeField, GroupSetting] private LayerMask _tgLayerMask;

        [Space]
        [Title("Canvas")]
        [SerializeField, GroupSetting] private CanvasGroup _canvasGroupMonsterReward;

        [SerializeField, GroupSetting] private CanvasGroup _canvasGroupMonsterHealth;
        [SerializeField, GroupSetting] private CanvasGroup _canvasGroupMonsterDamage;
        [SerializeField, GroupSetting] private LookAtCameraBubble _lookAtCameraRewardBubble;
        [SerializeField, GroupSetting] private LookAtCameraBubble _lookAtCameraBubbleHealth;
        [SerializeField, GroupSetting] private LookAtCameraBubble _lookAtCameraBubbleDamage;

        [Space]
        [Title("Boss")]
        [SerializeField, GroupSetting] private Image _iconBoss;


        public Transform ShotTarget => _shotTarget;

        public bool IsDead => _health.IsEmpty;


        [GroupView] private float _healthMultiplier;
        [GroupView] private float _damageMultiplier;
        [GroupView] private float _addedDamage;


        private Transform _target;
        private MonsterData _monsterData;
        private bool _isMove;
        private bool _inRun;

        private MainMachine _mainMachine;
        private Coroutine _canvasShowCoroutine;

        private Transform CachedTransform => _cachedTransform == null ? _cachedTransform = transform : _cachedTransform;
        private Transform _cachedTransform;
        private LevelData _currentLevelData;
        private bool _turn;
        private Transform _targetTransform;
        private Quaternion _updatedRotation;
        private MonsterChance _monsterChance;
        private bool _lastMonster;


        public void Init(Transform target, MonsterChance monsterChance, LevelData currentLevelData,
            float healthMultiplier, float damageMultiplier, float addedHealth, float addedDamage, int cycle,
            bool lastMonster)
        {
            _target = target;
            _monsterChance = monsterChance;
            _monsterData = monsterChance.Data;
            _currentLevelData = currentLevelData;
            _lastMonster = lastMonster;

            _addedDamage = addedDamage;

            _healthMultiplier = (monsterChance.Boss ? monsterChance.HealthMultiplier * cycle : 1) * healthMultiplier;
            _damageMultiplier = (monsterChance.Boss ? monsterChance.DamageMultiplier : 1) * damageMultiplier;


            MainMachine.OnPlayerDied += MakeHappy;
            _health.OnEmpty += Died;
            _monsterEvent.OnDiedEvent += StartCanvasCor;


            _monsterEvent.DieMonsterFalse();

            var health =
                _monsterData.MonsterHealthBase * _currentLevelData.MonsterHealthMultiplier * _healthMultiplier +
                addedHealth;
            _health.Init(health);

            Reset();


            _agent.enabled = true;
            _agent.ResetPath();
            SetTarget();
            CachedTransform.DOKill();
            if (monsterChance.Boss)
            {
                ShowCanvasHealth();
                EnableBossIcon(true);

                CachedTransform.DOScale(Vector3.one * 1.5f, 0);
                _lookAtCameraBubbleHealth.SetTransform(true);
            }
            else
            {
                EnableBossIcon(false);
                CachedTransform.DOScale(Vector3.one, 0);
                _lookAtCameraBubbleHealth.SetTransform(false);
            }
        }

        public void Release()
        {
            OnReleased?.Invoke(this);

            Reset();

            MainMachine.OnPlayerDied -= MakeHappy;
            _health.OnEmpty -= Died;
            _monsterEvent.OnDiedEvent -= StartCanvasCor;

            _poolItem.Release();
        }


        public void TakeDamage(float damage, float pushForce = 0f)
        {
            if (_health.IsEmpty) return;
            if (pushForce > 0)
            {
                StopAgent();
                PushMonster(pushForce);
            }


            _health.Decrease(damage);
            ShowCanvasHealth();
            _flasher.DoFlash();
        }

        public void MakeDamage()
        {
            if (IsDead)
            {
                Died();
                return;
            }

            var damage =
                _monsterData.MonsterDamageBase * _currentLevelData.MonsterDamageMultiplier * _damageMultiplier +
                _addedDamage;

            _mainMachine.TakeDamage(damage);
            if (gameObject.activeSelf)
            {
                StartCoroutine(ShowDamageCanvas(damage));
            }
        }

        private void FixedUpdate()
        {
            if (_turn && _targetTransform != null)
            {
                Vector3 target = _targetTransform.position - CachedTransform.position;
                Vector3 toTarget = new Vector3(target.x, 0f, target.z);
                _updatedRotation = Quaternion.Euler(0f, Quaternion.LookRotation(toTarget).eulerAngles.y, 0f);
                Quaternion rotation = Quaternion.Slerp(CachedTransform.rotation, _updatedRotation,
                    _durationTurn * Time.fixedDeltaTime);
                CachedTransform.rotation = rotation;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            var go = other.gameObject;
            if (!go.IsInLayer(_tgLayerMask)) return;
            if (!other.TryGetComponent(out _mainMachine)) return;


            _targetTransform = other.transform;
            if (_agent.isOnNavMesh) _agent.isStopped = true;
            _turn = true;
            _monsterEvent.SetSpeed(0);
            _monsterEvent.SetAttack(true);
        }

        private void OnTriggerExit(Collider other)
        {
            if (_health.IsEmpty) return;
            var go = other.gameObject;
            if (!go.IsInLayer(_tgLayerMask)) return;
            if (_agent.isOnNavMesh) _agent.isStopped = false;
            _turn = false;
            _monsterEvent.SetAttack(false);
            SetTarget();
        }
        
        private void OnDestroy()
        {
            _health.OnEmpty -= Died;
            MainMachine.OnPlayerDied -= MakeHappy;
            _monsterEvent.OnDiedEvent -= StartCanvasCor;

            _canvasGroupMonsterReward.DOKill();
            _canvasGroupMonsterDamage.DOKill();
            CachedTransform.DOKill();
        }

        private void ShowCanvasHealth()
        {
            _canvasGroupMonsterHealth.Show();
            _lookAtCameraBubbleHealth.SetSettings(_health.CurrentHealth, _health.MaxHealth);
        }

        private void EnableBossIcon(bool active)
        {
            _iconBoss.gameObject.SetActive(active);
        }

        private void PushMonster(float pushForce)
        {
            var direction = CachedTransform.position - CachedTransform.forward;
            _rb.AddForce(direction * pushForce, ForceMode.Impulse);
        }

        private IEnumerator ShowDamageCanvas(float damageValue)
        {
            _canvasGroupMonsterDamage.Show();
            _lookAtCameraBubbleDamage.SetSettings(damageValue);
            yield return new WaitForSeconds(_monsterData.DurationCanvas);
            _canvasGroupMonsterDamage.Hide();
        }

        private void Died()
        {
            _turn = false;
            StopAgent();
            _monsterEvent.DieMonster();
            OnDied?.Invoke(this);
        }

        private void StartCanvasCor()
        {
            StopCanvasCor();

            _canvasShowCoroutine = StartCoroutine(ShowCanvasCor());
        }

        private void StopCanvasCor()
        {
            if (_canvasShowCoroutine != null)
            {
                StopCoroutine(_canvasShowCoroutine);
                _canvasShowCoroutine = null;
            }
        }


        private IEnumerator ShowCanvasCor()
        {
            var rewardForEnemy = Mathf.RoundToInt(_monsterData.MonsterReward.RandomValue() *
                                                  UpgradeProvider.GetUpgradeValue(UpgradeType.IncomeKill));

            var reward = _monsterChance.Boss ? rewardForEnemy * _monsterChance.RewardMultiplier : rewardForEnemy;

            var currentAmount = (int) reward;
            _lookAtCameraRewardBubble.SetSettings(currentAmount, _monsterData.ResourceData.ResourceIcon);
            ResourceHandler.AddResource(_monsterData.ResourceData.Type, currentAmount, true);
            _canvasGroupMonsterHealth.Hide();
            _canvasGroupMonsterReward.Show(_monsterData.DurationCanvas);
            ParticleSpawn();
            yield return new WaitForSeconds(_monsterData.DurationMoveDown);
            _canvasGroupMonsterReward.Hide(_monsterData.DurationCanvas);
            CachedTransform.DOKill();
            CachedTransform.DOLocalMoveY(_monsterData.MonsterDieHeight, _monsterData.DurationMoveDown);
            _agent.enabled = false;
            yield return new WaitForSeconds(_monsterData.DurationMoveDown + 0.1f);
            Release();
        }

        private void ParticleSpawn() => Pool.Instance.Get(_monsterData.ParticleDied, _particleTransform.position);

        private void MakeHappy()
        {
            StopAgent();
            _turn = false;
            Invoke(nameof(MakeHappyEvent), Random.value);
        }

        private void MakeHappyEvent()
        {
            _monsterEvent.MakeHappy();
        }

        private void StopAgent()
        {
            if (!_agent.isOnNavMesh) return;
            _agent.isStopped = true;
            _agent.velocity = Vector3.zero;
            _monsterEvent.SetSpeed(0);
            _agent.ResetPath();
        }

        private void Reset()
        {
            _canvasGroupMonsterReward.Hide();
            _canvasGroupMonsterHealth.Hide();
            _canvasGroupMonsterDamage.Hide();
            StopCanvasCor();

            _rb.velocity = Vector3.zero;
        }

        private void SetTarget()
        {
            _agent.SetDestination(_target.position);


            var speed = _lastMonster || Random.value < 0.5f ? _monsterData.MonsterRunSpeed : _monsterData.MonsterWalkSpeed;

            _agent.speed = speed;
            _monsterEvent.SetSpeed(speed);
        }

       
    }
}