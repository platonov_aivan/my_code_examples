﻿using System;
using System.Collections;
using Common;
using DG.Tweening;
using Extensions;
using Gameplay.Characters;
using Gameplay.DragObject;
using Gameplay.HideCatsGame;
using Gameplay.Objects;
using Sirenix.OdinInspector;
using UI.ProgressBars;
using UnityEngine;

namespace UI.PlaceCanvas
{
    public class PlaceSearchCat : PlaceBase
    {
        public event Action<Player> OnFindCat;

        [Title("Components")]
        [SerializeField] private DragObjectElement _dragObjectElement;
        [SerializeField] private CanvasGroup _searchCanvas;
        [Title("Settings")]
        [SerializeField] private float _durationShow = 0.2f;
        [SerializeField] private float _endValueScale = 1.1f;
        [SerializeField] private float _durationScale = 0.8f;
        private Coroutine _startFindCatCor;
        private bool _isActiveSearch;
        private bool _isNeedScale;

        public void ScalePlace()
        {
            transform.DOScale(_endValueScale, _durationScale).SetLoops(-1, LoopType.Yoyo);
        }

        public void NeedScale(bool active)
        {
            _isNeedScale = active;
        }

        private void Awake()
        {
            _progressBar.SetMaxValue(_findDuration);
            HideCatGame.OnStartHideCatGame += EnableSearchZone;
            PlaceInteractionHeap.OnResourceOnPlace += CheckForEnablePlace;
        }

        private void OnDestroy()
        {
            HideCatGame.OnStartHideCatGame -= EnableSearchZone;
            PlaceInteractionHeap.OnResourceOnPlace -= CheckForEnablePlace;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Player player))
            {
                ProgressUp(player);
                SetAnimationModel(IsUp);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(out Player player))
            {
                ProgressDown();
                AnimationPlayer(player, false);
                SetAnimationModel(IsUp);
                _searchCanvas.Hide(_durationShow);

                if (_isNeedScale)
                {
                    ScalePlace();
                }
            }
        }

        private void CheckForEnablePlace(bool active)
        {
            if (!_isActiveSearch) return;
            EnablePlace(active);
        }

        private IEnumerator FindCatCor(Player player)
        {
            if (IsUp)
            {
                _searchCanvas.Show(_durationShow);
            }

            var timer = 0f;
            transform.DORewind();
            yield return new WaitForSeconds(_fillingDelay);
            AnimationPlayer(player, IsUp);
            while (true)
            {
                yield return null;

                CurrentValue = Math.Clamp(CurrentValue + Time.deltaTime * IsUp.Sign(), 0, _findDuration);
                _progressBar.SetValue(CurrentValue, true);

                if (CurrentValue <= 0) break;

                if (IsUp)
                {
                    timer += Time.deltaTime;
                    
                    if (timer >= 1)
                    {
                        MyVibration.Haptic(MyHapticTypes.Selection);
                        timer = 0f;
                    }
                }


                if (CurrentValue < _findDuration) continue;

                IsCleared = true;
                OnFindCat?.Invoke(player);
                EnableSearchZone(false, SetNormalScaleZone);
                AnimationPlayer(player, false);
                SetAnimationModel(false);
                CurrentValue = 0;
                _progressBar.SetValue(CurrentValue, true);
                break;
            }
        }

        private void SetNormalScaleZone()
        {
            _activeZone.SetNormalScale();
            NeedScale(false);
        }

        private void SetAnimationModel(bool active)
        {
            if (active)
            {
                _dragObjectElement.Model.DOKill();
                _dragObjectElement.Model.DOScale(_dragObjectElement.EndValueScale, _dragObjectElement.DurationScale)
                    .SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                _dragObjectElement.Model.DOKill();
                _dragObjectElement.Model.DOScale(1, _dragObjectElement.DurationScale);
            }
        }

        private void AnimationPlayer(Player player, bool active)
        {
            player.PlayLooking(active);
        }

        private void EnableSearchZone(bool active, Action action = default)
        {
            _isActiveSearch = active;
            EnablePlace(active, action);
        }

        private void EnablePlace(bool active, Action action = default)
        {
            if (active)
            {
                _progressBar.CanvasGroup.Show(_durationShow);
            }
            else
            {
                _searchCanvas.Hide(_durationShow);
                _progressBar.CanvasGroup.Hide(_durationShow, callback: () => action?.Invoke());
            }

            _collider.enabled = active;
        }

        private void ProgressUp(Player player)
        {
            IsUp = true;
            _startFindCatCor.Stop(this);
            _startFindCatCor = StartCoroutine(FindCatCor(player));
        }

        private void ProgressDown() => IsUp = false;
    }
}