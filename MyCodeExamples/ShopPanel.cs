﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace UI.WindowPanels
{
    public class ShopPanel : MonoBehaviour
    {
        [Title("Components")]
        [SerializeField] private MyButton _buttonShop;
        [SerializeField] private MyButton _bgButton;
        [SerializeField] private MyButton _exitButton;
        [SerializeField] private CanvasGroup _canvasGroup;
        [Title("Settings")]
        [SerializeField] private float _durationOpenCanvas = 0.2f;

        private void Awake()
        {
            HidePanel();
            Subscribe();
        }

        private void OnDestroy() => Unsubscribe();
        private void OpenPanel() => _canvasGroup.Show(_durationOpenCanvas);

        private void HidePanel() => _canvasGroup.Hide(_durationOpenCanvas);

        private void Subscribe()
        {
            _buttonShop.OnClick += OpenPanel;
            _bgButton.OnClick += HidePanel;
            _exitButton.OnClick += HidePanel;
        }

        private void Unsubscribe()
        {
            _buttonShop.OnClick -= OpenPanel;
            _bgButton.OnClick -= HidePanel;
            _exitButton.OnClick -= HidePanel;
        }
    }
}