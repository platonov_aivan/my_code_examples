﻿using System.Collections.Generic;
using Extensions;
using Gameplay.Characters;
using Gameplay.Characters.Movements;
using Gameplay.DragObject.Cats;
using Gameplay.GardenElement.GardenTriggers;
using Gameplay.HelperLogic.HelperState;
using Gameplay.HideCatsGame;
using Gameplay.Interaction;
using Gameplay.Providers;
using UI.PlaceCanvas;
using UnityEngine;
using static Common.Enums;

namespace Gameplay.HelperLogic
{
    public class Helper : CharacterBase
    {
        [SerializeField] private ParticleSystem _sleepingParticle;

        [GroupView] private WishType _currentCatWish = WishType.NotActivateType;
        [GroupView] private string CurrentState => _stateMachine?.NameOfState;
        [GroupView] private Transform _pointIdleSit;
        [GroupView] private List<Transform> _pointIdleList;
        [GroupView] private List<PlaceInteractionHeap> _placeInteractionHeaps;
        [GroupView] private List<PlaceInteractionWithSlot> _placeInteractionWithSlot;
        [GroupView] private bool _playerInGarden;
        [GroupView] private bool _isCatInGarden;

        public WishType CurrentCatWish => _currentCatWish;

        private StateMachine _stateMachine;
        private bool _isCompleteInteraction;

        private MoveToHeapState _moveToHeapState;


        public void Init(Transform pointIdleSit, List<Transform> pointIdleList,
            List<PlaceInteractionHeap> placeInteractionHeaps,
            List<PlaceInteractionWithSlot> placeInteractionWithSlots)
        {
            _pointIdleSit = pointIdleSit;
            _pointIdleList = pointIdleList;
            _placeInteractionHeaps = placeInteractionHeaps;
            _placeInteractionWithSlot = placeInteractionWithSlots;

            if (_movement is NavMeshAgentMovement navMeshAgentMovement)
            {
                navMeshAgentMovement.InitAgentData(this);
            }

            InitializeStateMachine();
        }

        private void InitializeStateMachine()
        {
            _stateMachine = new StateMachine();

            var idleBaseState = new IdleBaseState(this, _pointIdleSit, _pointIdleList, _sleepingParticle);
            var moveToHeapState = new MoveToHeapState(this, _placeInteractionHeaps);
            _moveToHeapState = moveToHeapState;
            var moveToSlotState = new MoveToSlotState(this, _placeInteractionWithSlot);
            var interactionSlotState = new InteractionSlotState(this, _placeInteractionWithSlot);
            var sleepingState = new SleepingState(this, _sleepingParticle, _pointIdleSit);
            var playerInGardenState = new PlayerInGardenState(this, _pointIdleList, _sleepingParticle);
            var catInGardenState = new CatInGardenState(this, _pointIdleList, _sleepingParticle);

            _stateMachine.AddTransition(idleBaseState, sleepingState, IsIdleTargetReached);

            _stateMachine.AddTransition(idleBaseState, moveToHeapState, IsNeedMoveToInteractionHeap);
            _stateMachine.AddTransition(sleepingState, moveToHeapState, IsNeedMoveToInteractionHeap);

            _stateMachine.AddTransition(moveToHeapState, moveToSlotState, IsHasItem);
            _stateMachine.AddTransition(moveToHeapState, idleBaseState, IsNotWishes);
            _stateMachine.AddTransition(moveToHeapState, playerInGardenState, IsPlayerInGarden);
            _stateMachine.AddTransition(moveToHeapState, catInGardenState, IsCatInGarden);

            _stateMachine.AddTransition(moveToSlotState, interactionSlotState, IsInteractionTargetReached);
            _stateMachine.AddTransition(moveToSlotState, playerInGardenState, IsPlayerInGarden);
            _stateMachine.AddTransition(moveToSlotState, catInGardenState, IsCatInGarden);


            _stateMachine.AddTransition(interactionSlotState, idleBaseState, IsCompleteInteraction);
            _stateMachine.AddTransition(interactionSlotState, playerInGardenState, IsPlayerInGarden);
            _stateMachine.AddTransition(interactionSlotState, catInGardenState, IsCatInGarden);

            _stateMachine.AddTransition(playerInGardenState, idleBaseState, IsNotPlayerInGarden);
            _stateMachine.AddTransition(catInGardenState, idleBaseState, IsNotCatInGarden);


            bool IsNeedMoveToInteractionHeap() => WishSetupProvider.CheckForWish(_characterData.RoomTypeHelper);
            bool IsHasItem() => MyHeap.Count > 0;
            bool IsNotWishes() => WishSetupProvider.CheckForNotWish(_characterData.RoomTypeHelper);
            bool IsIdleTargetReached() => idleBaseState.IsMove && AgentReached();
            bool IsInteractionTargetReached() => AgentReached();
            bool IsCompleteInteraction() => _isCompleteInteraction;

            bool IsPlayerInGarden() => _playerInGarden;
            bool IsNotPlayerInGarden() => !_playerInGarden;

            bool IsCatInGarden() => _isCatInGarden;
            bool IsNotCatInGarden() => !_isCatInGarden;


            _stateMachine.SetState(idleBaseState);
        }

        private void Awake()
        {
            PlaceInteractionWithSlot.OnHelperEndInteraction += IsComplete;
            GardenTrigger.OnEnterGarden += PlayerEnterGarden;
            DraggableCat.OnRunawayGardenСat += CatEscapeGarden;

            if (HideCatGame.CountCatInGarden > 0)
            {
                _isCatInGarden = true;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            PlaceInteractionWithSlot.OnHelperEndInteraction -= IsComplete;
            GardenTrigger.OnEnterGarden -= PlayerEnterGarden;
            DraggableCat.OnRunawayGardenСat -= CatEscapeGarden;
        }

        private void Update()
        {
            _stateMachine.Tick();

            if (_currentCatWish == _moveToHeapState.CurrentWish) return;
            {
                _currentCatWish = _moveToHeapState.CurrentWish;
            }
        }

        private void CatEscapeGarden(bool catInGarden)
        {
            if (catInGarden)
            {
                _isCatInGarden = true;
            }
            else
            {
                if (HideCatGame.CountCatInGarden > 0)
                {
                    _isCatInGarden = true;
                    return;
                }

                _isCatInGarden = false;
            }
        }

        private void PlayerEnterGarden(bool playerInGarden, CharacterBase _)
        {
            if (_isCatInGarden) return;
            _playerInGarden = playerInGarden;
        }

        private bool AgentReached()
        {
            return _movement is NavMeshAgentMovement navMeshAgentMovement
                ? !navMeshAgentMovement.Agent.pathPending
                    ? navMeshAgentMovement.Agent.IsReached()
                    : default
                : default;
        }

        private void IsComplete(bool complete)
        {
            _isCompleteInteraction = complete;
        }
    }
}