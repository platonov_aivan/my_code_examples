﻿using System;
using System.Collections.Generic;
using Common;
using Extensions;
using Gameplay.CameraSwitcher;
using Gameplay.Places.EndLevel;
using ScriptableObjects.Classes.ItemShop;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gameplay.Shop
{
    public class Shelving : MonoBehaviour
    {
        [SerializeField] private List<Shelf> _shelves;
        [Title("Non-exhibition items")]
        [SerializeField] private List<ItemShopData> _notExhibitionItems;

        [GroupView] private readonly List<ItemShopData> _removedList = new();

        
        
        private void Awake()
        {
            MiniGameSwitcher.OnStartSwitchGameInLevel += Init;
            EndLevelPlace.OnFinish += SaveItems;
            Shelf.OnItemRemove += RemoveItem;
        }

        private void OnDestroy()
        {
            MiniGameSwitcher.OnStartSwitchGameInLevel -= Init;
            EndLevelPlace.OnFinish -= SaveItems;
            Shelf.OnItemRemove -= RemoveItem;
        }

        private void SaveItems()
        {
            foreach (var itemShopData in _removedList)
            {
                SaveItem(itemShopData);
            }
        }

        private void RemoveItem(ItemShopData item)
        {
            _removedList.Add(item);
        }

        private void SaveItem(ItemShopData item)
        {
            var removedItem = new BoolDataValueSavable($"{item.ItemShopType.ToString()}");
            removedItem.Value = true;
            removedItem.Save();
        }

        private void Init()
        {
            var j = 0;
            for (var i = 0; i < _shelves.Count; i++)
            {
                ItemShopData data;
                bool flag;
                do
                {
                    if (j >= _notExhibitionItems.Count)
                    {
                        _shelves[i].DeactivateArea();
                        flag = false;
                        data = default;
                    }
                    else
                    {
                        data = _notExhibitionItems[j];
                        var removedItem = new BoolDataValueSavable($"{data.ItemShopType.ToString()}");
                        flag = removedItem.Value;
                    }

                    j++;
                }
                while (flag);

                _shelves[i].Init(data);
            }
        }
    }
}