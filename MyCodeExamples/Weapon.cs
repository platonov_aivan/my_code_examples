﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Extensions;
using Game.Scripts.Utils.ObjectPool;
using Gameplay.Machine;
using Gameplay.Monsters;
using Gameplay.Weapons.Logic;
using ScriptableObjects.Classes.WeaponsMachine;
using UI;
using UI.WindowUI.Upgrades.ButtonUlt;
using UnityEngine;
using static Common.Enums;

namespace Gameplay.Weapons.View
{
    public class Weapon : MonoBehaviour
    {
        [Space]
        [Header("Transform")]
        [SerializeField, GroupComponent] private Transform _tower;

        [SerializeField, GroupComponent] private Transform _gun;
        [SerializeField, GroupComponent] private MeshFilter _meshTower;
        [SerializeField, GroupComponent] private MeshFilter _meshGun;

        [Header("Logic Weapons")]
        [SerializeField, GroupComponent] private MachineGuns _machineGuns;

        [SerializeField, GroupComponent] private Lasers _lasers;

        [Header("Trigger")]
        [SerializeField, GroupComponent] private Collider _collider;

        [Header("Ray")]
        [SerializeField, GroupComponent] private LayerMask _checkMask;

        [SerializeField, GroupComponent] private LayerMask _monsterLayer;
        [SerializeField, GroupComponent] private float _dist = float.MaxValue;


        [Header("Weapon View Data")]
        [SerializeField, GroupComponent] private UpdateMeshViewData _updateMeshViewData;

        [Header("Particle Main")]
        [SerializeField, GroupComponent] private Transform _mainParticleTransform;


        [GroupView] private readonly HashSet<Monster> _monstersList = new();


        private Transform CachedTransform => _cachedTransform ??= transform;
        private Transform _cachedTransform;

        private Coroutine _shootCor;
        private Monster _currentMonster;
        private bool _ultActive;
        private Coroutine _defaultRotateCor;
        private bool _destroyed;
        private Vector3 _startGunPos;
        private bool _isCanSpawnParticle;


        private void Awake()
        {
            _destroyed = false;

            _startGunPos = _gun.transform.localPosition;
            Subscribe();
        }

        private void Start()
        {
            UpdateMesh();
            SwitchTrigger();
        }

        private void OnDestroy()
        {
            _destroyed = true;

            Unsubscribe();
            ClearAll();
        }

        private void Subscribe()
        {
            MainMachine.OnPlayerDied += StopAllCor;
            MainMachine.OnCanSpawnParticle += CanSpawnParticle;
            UpgradeProvider.OnProgressUpdated += UpdateWeapon;
            UpgradeProvider.OnUnlock += UnlockWeapon;
            _machineGuns.OnRecoil += ShotRecoil;

            UltPanel.OnStartUlt += ActiveUlt;
            UltPanel.OnFinishUlt += ActiveUlt;

            SceneUI.OnUpdateLosingWindow += ClearAll;
        }

        private void Unsubscribe()
        {
            MainMachine.OnPlayerDied -= StopAllCor;
            MainMachine.OnCanSpawnParticle -= CanSpawnParticle;
            UpgradeProvider.OnProgressUpdated -= UpdateWeapon;
            UpgradeProvider.OnUnlock -= UnlockWeapon;
            _machineGuns.OnRecoil -= ShotRecoil;

            UltPanel.OnStartUlt -= ActiveUlt;
            UltPanel.OnFinishUlt -= ActiveUlt;

            SceneUI.OnUpdateLosingWindow -= ClearAll;

        }

        private void CanSpawnParticle(bool active)
        {
            _isCanSpawnParticle = active;
        }

        private void ActiveUlt(UltType type, UltData data, bool active)
        {
            if (type == UltType.Lasers)
            {
                _ultActive = active;
                _lasers.Enable(active);

                if (active)
                {
                    _machineGuns.StopShotCor();
                    _lasers.Init(data);
                }
                else
                {
                    _lasers.StopShotCor();
                }
            }
        }

        private void ClearAll()
        {
            _gun.DOKill();
            _monstersList.Clear();

            StopShoot();
        }

        private void StopShoot()
        {
            StopAllCor();
            StopGetCloseMonsterCor();
        }

        private void StopAllCor()
        {
            _lasers.StopShotCor();
            _machineGuns.StopShotCor();
            StopShotCor();
            StopRotateCor();
        }

        private void SwitchTrigger()
        {
            var upgradeLevel = UpgradeProvider.GetUpgradeLevel(_machineGuns.TypeDamage);
            _collider.enabled = upgradeLevel > 0;
        }


        private void UnlockWeapon(List<UpgradeType> weapon)
        {
            foreach (var upgradeType in weapon)
            {
                if (upgradeType == _machineGuns.TypeDamage)
                {
                    GunActive(true);
                    _machineGuns.UpdateReload();
                    _collider.enabled = true;
                }
            }
        }

        private void UpdateWeapon(UpgradeType type)
        {
            if (type == _machineGuns.TypeDamage)
            {
                ParticleSpawn();
            }
            else if (type == _machineGuns.TypeSpeed)
            {
                _machineGuns.UpdateSpeed();
                ParticleSpawn();
            }
            else if (type == _machineGuns.TypePermanent)
            {
                UpdateMesh();
                ParticleSpawn();
            }
        }

        private void UpdateMesh()
        {
            var upgradeLevel = UpgradeProvider.GetUpgradeLevel(_machineGuns.TypePermanent);

            Mesh mesh = null;
            foreach (var keyValuePair in _updateMeshViewData.MainChangeDictionary)
            {
                if (keyValuePair.Key <= upgradeLevel)
                {
                    mesh = keyValuePair.Value;
                }
            }

            if (mesh != null && !_meshTower.mesh.name.StartsWith(mesh.name))
            {
                _meshTower.mesh = mesh;
            }

            mesh = null;
            foreach (var keyValuePair in _updateMeshViewData.SecondaryChangeDictionary)
            {
                if (keyValuePair.Key <= upgradeLevel)
                {
                    mesh = keyValuePair.Value;
                }
            }

            if (mesh != null && !_meshGun.mesh.name.StartsWith(mesh.name))
            {
                _meshGun.mesh = mesh;
            }
        }


        private void ParticleSpawn()
        {
            if (_isCanSpawnParticle)
                Pool.Instance.Get(_updateMeshViewData.ParticleUpgrade, _mainParticleTransform.position);
        }


        private void GunActive(bool active)
        {
            _tower.gameObject.SetActive(active);
            _gun.gameObject.SetActive(active);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out Monster monster)) return;
            _monstersList.Add(monster);
            monster.OnDied += RemoveFromList;


            StartGetCloseMonsterCor();
        }

        private void StartGetCloseMonsterCor()
        {
            StopRotateCor();
            _shootCor ??= StartCoroutine(GetCloseMonsterCor());
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.TryGetComponent(out Monster monster)) return;
            RemoveFromList(monster);
        }

        private void RemoveFromList(Monster monster)
        {
            monster.OnDied -= RemoveFromList;
            RemoveMonster(monster);
        }

        private void RemoveMonster(Monster monster)
        {
            if (!_monstersList.Contains(monster)) return;
            _monstersList.Remove(monster);
            GetCloseMonster();
            if (_monstersList.Count > 0) return;
            StopShoot();
        }

        private IEnumerator GetCloseMonsterCor()
        {
            while (true)
            {
                yield return null;
                if (_currentMonster == null || _currentMonster.IsDead)
                {
                    StopShoot();
                    continue;
                }

                RotateTower(_currentMonster.ShotTarget.position, 10f);
                CheckAimed();

                if (!_ultActive)
                {
                    _machineGuns.SetCurrentMonster(_currentMonster);
                }
                else
                {
                    _lasers.SetCurrentMonster(_currentMonster);
                }
            }
        }

        private void RotateTower(Vector3 pos, float speed)
        {
            _tower.HorizontalSoftLookAt(pos, speed);
            _gun.VerticalSoftLookAt(pos);
        }

        private void CheckAimed()
        {
            var towerForward = _tower.forward.XZOnly();
            var currenMonsterForward =
                (_tower.position.XZOnly() - _currentMonster.ShotTarget.position.XZOnly()).normalized;
            var aimCoefficient = Vector3.Dot(towerForward, currenMonsterForward);
            _machineGuns.Aimed = aimCoefficient < -0.99f;
            _lasers.Aimed = aimCoefficient < -0.99f;
        }


        private void ShotRecoil(float recoilPower, float recoilDuration)
        {
            _gun.DOLocalMoveZ(_startGunPos.z - recoilPower, recoilDuration)
                .OnComplete(() => _gun.DOLocalMoveZ(_startGunPos.z, recoilDuration));
        }

        private void StopGetCloseMonsterCor()
        {
            if (_destroyed) return;
            _defaultRotateCor = StartCoroutine(RotateTowerCor());
        }

        private void StopShotCor()
        {
            if (_shootCor != null) StopCoroutine(_shootCor);
            _shootCor = null;
        }

        private void StopRotateCor()
        {
            if (_defaultRotateCor != null) StopCoroutine(_defaultRotateCor);
        }

        private IEnumerator RotateTowerCor()
        {
            while (true)
            {
                yield return null;
                var pos = _gun.position + CachedTransform.forward;
                RotateTower(pos, 3f);

                GetCloseMonster();
                if (_currentMonster != null)
                {
                    StartGetCloseMonsterCor();
                }
            }
        }


        private void GetCloseMonster()
        {
            _currentMonster = null;
            var minDistance = float.MaxValue;

            foreach (var monster in _monstersList)
            {
                var distance = monster.SqrDistanceTo(CachedTransform);

                if (distance < minDistance)
                {
                    if (monster.IsDead) continue;
                    var shotTargetPosition = monster.ShotTarget.position - CachedTransform.position;
                    Ray ray = new Ray(CachedTransform.position, shotTargetPosition);
#if UNITY_EDITOR
                    Debug.DrawRay(CachedTransform.position, shotTargetPosition);
#endif
                    if (Physics.Raycast(ray, out RaycastHit hit, _dist, _checkMask))
                    {
                        if (LayerMask.GetMask(LayerMask.LayerToName(hit.transform.gameObject.layer)) == _monsterLayer)
                        {
                            _currentMonster = monster;
                            minDistance = distance;
                        }
                    }
                }
            }
        }
    }
}