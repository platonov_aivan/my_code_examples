﻿using System.Collections.Generic;
using DG.Tweening;
using Extensions;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gameplay.FlashingMesh
{
    public class FlasherList : Flasher
    {
        [SerializeField] private bool _isFlasherList;
        [SerializeField, GroupComponent(false),ShowIf(nameof(_isFlasherList))] private List<MeshRenderer>        _meshRenderers;
        [SerializeField, GroupComponent(false),ShowIf(nameof(_isFlasherList))] private List<SkinnedMeshRenderer> _skinnedMeshRenderers;

        public override void DoFlash()
        {
            if (IsDelay) return;
            IsDelay = true;
            if (_meshRenderers != null) FlashListMesh(_meshRenderers);
            if (_skinnedMeshRenderers != null) FlashListSkinnedMesh(_skinnedMeshRenderers);
            
            Invoke(nameof(CooldownDelayFlashing), _cooldown);
        }

        private void CooldownDelayFlashing()
        {
            IsDelay = false;
        }

        private void FlashListMesh(List<MeshRenderer> list)
        {
            for (var i = 0; i < list.Count; i++)
            {
                list[i].material .DOKill();
                var i1 = i;
                list[i].material .DOColor(_flashColor, "_EmissionColor", _flashDuration)
                    .OnComplete(() => list[i1].material .DOColor(Color.clear, "_EmissionColor", _flashDuration));
            }
        }
        private void FlashListSkinnedMesh(List<SkinnedMeshRenderer> list)
        {
            for (var i = 0; i < list.Count; i++)
            {
                list[i].material .DOKill();
                var i1 = i;
                list[i].material .DOColor(_flashColor, "_EmissionColor", _flashDuration)
                    .OnComplete(() => list[i1].material .DOColor(Color.clear, "_EmissionColor", _flashDuration));
            }
        }

    }
}